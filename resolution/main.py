import re
import copy
import pprint


class Resolve:
    def __init__(self):
        self.kb = []
        self.variable_regex = re.compile(r'\b[a-z]\w*\b')
        self.func_regex = re.compile(r'(~?[A-Z]\w*)\s*\(')
        self.clauses = set()
        self.initial_length = 0
        self.steps = []
        self.query = ""
        self.count = 0

    def standardize(self, line, count):
        variables = list(set(re.findall(self.variable_regex, line)))
        for i in variables:
            line = line.replace(i, i + str(count))
        return line

    def negate(self, term):
        if term[0] == '~':
            return term[1:]
        return '~' + term

    def is_var(self, symbol):
        return isinstance(symbol, str) and \
            self.variable_regex.fullmatch(symbol) is not None

    def parse(self, term):
        if '(' not in term: return term

        match = self.func_regex.search(term)
        x = (match.group(1), []) #tuple with (func_name, [args])
        args = term[match.end(1)+1:-1].split(',')
        x[1].extend(self.parse(a) for a in args)
        return x

    def unparse(self, term):
        if isinstance(term, str): return term
        return term[0] + '(' + ','.join(self.unparse(x) for x in term[1]) + ')'

    def unify(self, a, b, theta):
        if theta == None: return None
        if a == b: return theta
        if self.is_var(a): return self.unify_var(a, b, theta)
        if self.is_var(b): return self.unify_var(b, a, theta)
        if isinstance(a, tuple) and isinstance(b, tuple):
            return self.unify(a[1], b[1], self.unify(a[0], b[0], theta))
        if isinstance(a, list) and isinstance(b, list):
            return self.unify(a[1:], b[1:], self.unify(a[0], b[0], theta))

    def unify_var(self, a, b, theta):
        if a in theta: return self.unify(theta[a], b, theta)
        if self.is_var(b) and b in theta: return self.unify(a, theta[b], theta)
        if self.occur_check(a, b): return None
        else: theta[a] = b; return theta

    def occur_check(self, var, val):
        if self.is_var(val):
            return var == val
        if isinstance(val, tuple):
            return self.occur_check(var, val[1])
        elif isinstance(val, list):
            if len(val) == 0:
                return False
            return self.occur_check(var, val[0]) and self.occur_check(var, val[1:])


    def test_unify(self):
        def u(a, b):
            theta = self.unify(a, b, {})
            if theta is None:
                return None
            result = {}
            for k, v in theta.items():
                result[k] = self.unparse(v)
            return result

        print()
        print('UNIFY TESTS')
        print('Constant, Constant', u('Constant', 'Constant'))
        print('Constant, OtherConstant', u('Constant', 'OtherConstant'))
        print('x1, Constant', u('x1', 'Constant'))
        print('Constant, x1', u('Constant', 'x1'))
        print('x1, x1', u('x1', 'x1'))
        print('x1, x2', u('x1', 'x2'))
        print('x1, F(x2)', u('x1', ('F', ['x2'])))
        print('Cat(Tuna), Animal(Tuna)', u(('Cat', ['Tuna']), ('Animal', ['Tuna'])))
        print('Kills(Curiosity, Tuna), Kills(Curiosity, Tuna)', u(('Kills', ['Curiosity', 'Tuna']), ('Kills', ['Curiosity', 'Tuna'])))
        print('Kills(Curiosity, Tuna), Kills(Curiosity, Jack)', u(('Kills', ['Curiosity', 'Tuna']), ('Kills', ['Curiosity', 'Jack'])))
        print('Loves(x1, F(x1)), Loves(Jack, x2)', u(('Loves', ['x1', ('F', ['x1'])]), ('Loves', ['Jack', 'x2'])))

    def swap_variables(self, clause, variables):
        text = ' | '.join(self.unparse(term) for term in clause)
        for key, value in variables.items():
            value = self.unparse(value)
            text = text.replace(key, value)
        return text if text is not None else ''

    def resolve(self, left, right):
        out = set()
        l_funcs = set()
        r_funcs = set()
        l_funcs.update([i[0].replace('~', '') for i in left])
        r_funcs.update([i[0].replace('~', '') for i in right])
        if l_funcs <= r_funcs:
            for i in left:
                i = (self.negate(i[0]), i[1])
                for j in right:
                    u = self.unify(i, j, {})
                    if u is not None:
                        clause = copy.copy(right)
                        clause.remove(j)
                        out.add(self.swap_variables(clause, u))
        if len(out) > 0:
            self.steps.append((left, right))
        return out

    def gen_clauses(self):
        out = set()
        for i in self.kb:
            out.add(' | '.join(self.unparse(term) for term in i))
        return out

    def to_kb(self, clauses):
        out = []
        for i in clauses:
            tmp = list(s.replace(' ', '') for s in i.split('|'))
            out.append(list(resolution.parse(term) for term in tmp))
        return out

    def resolution(self):
        clauses = self.gen_clauses()
        new = set()
        while True:
            for i in self.kb:
                for j in self.kb:
                    if i != j:
                        resolvents = self.resolve(i, j)
                        if '' in resolvents and (i == self.query or j == self.query):
                            clauses = clauses | new
                            self.kb = self.to_kb(clauses)
                            return True
                        resolvents.discard('')
                        new = new | resolvents
            if new <= clauses:
                clauses = clauses | new
                self.kb = self.to_kb(clauses)
                return False
            clauses = clauses | new
            self.kb = self.to_kb(clauses)




if __name__ == '__main__':
    resolution = Resolve()
    sentences = []

    def add(line):
        x = list(s.replace(' ', '') for s in line.split('|'))
        sentences.append(x)

    # fname = input('Input File: ')

    fname = input("Please Enter a Filename: ")
    with open(fname) as f:
        for line in f:
            line = resolution.standardize(line, resolution.count)
            resolution.count += 1
            if '?' in line:
                add(resolution.negate(line.replace("?", "").strip()))
                resolution.query = list(resolution.parse(term) for term in sentences[-1])
            elif '.' in line:
                add(line.replace('.', '').strip())

    for s in sentences:
        resolution.kb.append(list(resolution.parse(term) for term in s))

    # print('IN INPUT FORM')
    # for s in resolution.kb:
    #     print(' | '.join(resolution.unparse(term) for term in s))
    #
    # print()
    # print('INTERNAL REPRESENTATION')
    # for s in resolution.kb:
    #     print(s)
    #
    # resolution.test_unify()
    kb = copy.copy(resolution.kb)
    # print(resolution.resolution())
    if resolution.resolution():
        print("True")
        print()
        print("Steps taken:")
        for i in resolution.steps:
            # print(' | '.join(resolution.unparse(i[0])) + '&' + ' | '.join(resolution.unparse(i[1])))
            print(' | '.join(resolution.unparse(j) for j in i[0]) + ' & ' + ' | '.join(resolution.unparse(k) for k in i[1]))
        print()
        print("Clauses added:")
        clauses = [x for x in resolution.kb if x not in kb]
        for i in clauses:
            print(' | '.join(resolution.unparse(j) for j in i))
    else:
        print("False")
