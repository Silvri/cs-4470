import string
import random
import pprint
from collections import defaultdict
import sys
import time


class Genetics:
    # self.goal = raw_input("Please enter a self.goal string: ")
    def __init__(self, word):

        # Config options
        self.goal = word
        self.pop_size = 500
        self.fitness_threshold = 1.0
        self.crossover = self.crossover_single
        self.fitness = self.fitness_ascii
        self.cross_choice = self.rank_selection
        self.r = 0.75
        self.m = 0.35

        # Default Config
        # self.goal = word
        # self.pop_size = 500
        # self.fitness_threshold = 1.0
        # self.crossover = self.crossover_single
        # self.fitness = self.fitness_ascii
        # self.cross_choice = self.fitness_proportionate
        # self.r = 0.75
        # self.m = 0.35

        # Non cofig options
        self.dna_size = len(self.goal)
        self.max_fitness = 0
        self.prob_table = self.gen_prob_table()

    def gen_prob_table(self):
        """Generate probability of charecters in word"""
        d = defaultdict(lambda: 0, {})
        for i in self.goal:
            d[i] += 1
        out = defaultdict(lambda: 0, {})
        pprint.pprint(d)
        for key, value in d.iteritems():
            out[key] = float(value) / float(self.dna_size)
        return out

    def random_char(self):
        return random.choice(string.ascii_letters)

    def gen_population(self):
        """Generate a random population."""
        return [''.join([self.random_char()
                for x in range(self.dna_size)]) for y in range(self.pop_size)]

    def fitness_ascii(self, word):
        """Returns the fitness value of a given word."""
        return sum([abs(ord(self.goal[x]) - ord(word[x])) for x in range(self.dna_size)])

    def fitness_prob(self, word):
        """Return the fitness value of a given word."""
        return int(abs(sum([self.prob_table[i]*100 for i in word]) - 100))

    def crossover_single(self, p1, p2):
        """Returns the single point crossover of two parents"""
        point = random.randint(0, self.dna_size - 1)
        return (p1[:point] + p2[point:], p2[:point] + p1[point:])

    def crossover_two(self, p1, p2):
        """Returns the two point crossover of two parents"""
        length = random.randint(1, (self.dna_size / 2))
        point = random.randint(1, (self.dna_size / 2))
        return (p1[:point] + p2[point: point+length] + p1[point+length:],
                p2[:point] + p1[point: point+length] + p2[point+length:])

    def mutate(self, word):
        """Returns the mutation of a word"""
        point = random.randint(0, self.dna_size - 1)
        out = [x for x in word]
        out[point] = self.random_char()
        return ''.join(out)

    def calc_fit(self, pop):
        """Returns the weighted population and sets self.max_fitness"""
        weighted = []
        for i in pop:
            fit = self.fitness(i)
            if fit == 0.0:
                self.max_fitness = 1.0
                pair = (i, 1.0)
            else:
                pair = (i, .99/fit)
                self.max_fitness = pair[1] if pair[1] > self.max_fitness else self.max_fitness
            weighted.append(pair)
        return weighted

    def fitness_proportionate(self, w_pop):
        """Returns a choice from the weighted population"""
        total = sum((i[1] for i in w_pop))
        n = random.uniform(0, total)
        out = 0
        for i in range(len(w_pop)):
            out = i
            if n < w_pop[i][1]:
                return w_pop.pop(i)[0]
            n = n - w_pop[i][1]
        return w_pop.pop(out)[0]

    def tournament_selection(self, w_pop):
        """Returns a choice from the weighted population"""
        while True:
            c1 = w_pop[random.randint(0, len(w_pop) - 1)]
            c2 = w_pop[random.randint(0, len(w_pop) - 1)]
            if c1[0] != c2[0]:
                break
        return c1[0] if c1[1] < c2[1] else c2[0]

    def rank_selection(self, w_pop):
        """Return a choice from the weighted population"""
        sorted_list = sorted(w_pop, key=lambda tup: tup[1])
        out = sorted_list.pop()[0]
        return out

    def evolve(self):
        print("Evolving....")
        time1 = time.time()
        population = self.gen_population()
        weighted = self.calc_fit(population)
        generations = 1
        crossovers = 0
        mutations = 0
        while self.max_fitness < self.fitness_threshold:
            sys.stdout.flush()
            sys.stdout.write("Generation: %d\r" % generations)
            population = []
            # Select items for crossover
            for i in range(int(self.r*self.pop_size) / 2):
                p1 = self.cross_choice(weighted)
                p2 = self.cross_choice(weighted)

                # crossover
                c1, c2 = self.crossover(p1, p2)
                crossovers += 1
                population.append(c1)
                population.append(c2)

            # Add back the rest of the population
            for j in weighted:
                population.append(j[0])

            # mutate population
            for j in range(int(self.m*self.pop_size)):
                population[j] = self.mutate(population[i])
                mutations += 1

            # calculate fitness
            weighted = self.calc_fit(population)
            generations += 1
        time2 = time.time()
        sorted_list = sorted(weighted, key=lambda tup: tup[1])
        sys.stdout.flush()
        print("Generation: %d" % (generations - 1))
        print("Crossovers: %d" % crossovers)
        print("Mutations: %d" % mutations)
        print("Time: {}ms".format((time2 - time1)*1000))
        return sorted_list.pop()[0]


if __name__ == '__main__':
    # Generate random population
    print len(sys.argv)
    if len(sys.argv) < 2:
        word = raw_input("Please enter an input string: ")
    else:
        word = sys.argv[1]
    genetic = Genetics(word)
    print("Found %s: " % genetic.evolve())
    print("")
