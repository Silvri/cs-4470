# Author: Daniel Clark
import random
from collections import deque

random.seed()

_solvedId = '12345678_'
_offset = {'up': -3, 'right': 1, 'down': 3, 'left': -1}
_actions = {
                0: ['right', 'down'],
                1: ['right', 'down', 'left'],
                2: ['down', 'left'],
                3: ['up', 'right', 'down'],
                4: ['up', 'right', 'down', 'left'],
                5: ['up', 'down', 'left'],
                6: ['up', 'right'],
                7: ['up', 'right', 'left'],
                8: ['up', 'left'],
            }

_opposites = {'up': 'down', 'down': 'up', 'left': 'right', 'right': 'left'}

class State:
    @staticmethod
    def Solved():
        return State(_solvedId, 8)

    def __init__(self, id, zero):
        self.id = id
        self.zero = zero

    def children(self):
        return [self.nextState(a) for a in self.validActions()]

    def nextState(self, action):
        if action in self.validActions():
            s = list(self.id)
            z = self.zero
            idx = z + _offset[action]
            s[z], s[idx] = s[idx], s[z]
            return State(''.join(s), idx)

    def validActions(self):
        return _actions[self.zero]

    def isSolvedState(self):
        return self.id == _solvedId

    def __str__(self):
        return '{0} {1} {2}\n{3} {4} {5}\n{6} {7} {8}'.format(*self.id)

class Puzzle:
    def __init__(self, state=_solvedId):
        self.state = State(state, state.index('_'))

    def shuffle(self):
        for x in range(100):
            moves = self.validActions()
            r = random.randrange(len(moves))
            self.move(moves[r])

    def move(self, action):
        self.state = self.state.nextState(action)

    def validActions(self):
        return self.state.validActions()

    def display(self):
        print(str(self.state))

    def solved(self):
        return self.state.isSolvedState()

class Node:
    def __init__(self, state, cost=0, parent=None, action=None, depth=0):
        self.state = state
        self.cost = cost
        self.parent = parent
        self.action = action
        self.depth = depth

    def children(self):
        return [self.nextState(a) for a in self.state.validActions()]

    def nextState(self, action):
        return Node(self.state.nextState(action), self.cost + 1, self, action, self.depth + 1)

    def path(self):
        return list(reversed(list(self._chainGen())))

    def invertedPath(self):
        path = list(self._chainGen())
        for i in range(len(path) - 1, 0, -1):
            path[i].parent = path[i - 1]
            path[i].action = _opposites[path[i - 1].action]

        path[0].parent = None
        path[0].action = None
        return path

    def _chainGen(self):
        node = self
        while (node != None):
            yield node
            node = node.parent

    @property
    def key(self):
        return self.state.id

    # def __str__(self):
    #     return str(self.state)
    def __str__(self):
        """Establish str representatoin of object."""
        return "{0} {1} {2}\n{3} {4} {5}\n{6} {7} {8}".format(
                                                                self.state.id[0],
                                                                self.state.id[1],
                                                                self.state.id[2],
                                                                self.state.id[3],
                                                                self.state.id[4],
                                                                self.state.id[5],
                                                                self.state.id[6],
                                                                self.state.id[7],
                                                                self.state.id[8])

    def __hash__(self):
        return hash(self.state.id)

    def __eq__(self, other):
        return other is not None and self.state.id == other.state.id
