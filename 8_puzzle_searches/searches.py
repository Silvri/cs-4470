from puzzle import Puzzle, State, Node
from collections import deque
from pqueue import pqueue

class Solution:
    def __init__(self, path, expanded):
        self.path = path
        self.expanded = expanded
        if len(path) > 40:
            self.moves = "\n- - -\n".join([str(n) for n in reversed(self.path[-40:])])
        else:
            self.moves = "\n- - -\n".join([str(n) for n in reversed(self.path)])

    def __str__(self):
        s = 'Start State:\n{0}\n'.format(self.path[0].state)
        s += 'Expanded: {0}\nMoves: \n{1}\n'.format(self.expanded, self.moves)
        s += 'End State:\n{0}\n'.format(self.path[-1].state)
        return s


def depth_first(puz):
    expanded = 0
    start = Node(puz.state)
    frontier = [start]
    frontierSet = set([start])
    explored = set()

    while True:
        if len(frontier) == 0:
            return "No Solution"
        node = frontier.pop()
        frontierSet.remove(node)
        explored.add(node)
        expanded += 1

        for child in node.children():
            if child not in explored and child not in frontierSet:
                if child.state.isSolvedState():
                    return Solution(child.path(), expanded)
                frontier.append(child)
                frontierSet.add(child)

def depth_limited(puz, limit):
    expanded = 0
    start = Node(puz.state)
    frontier = [start]
    frontierSet = set([start])
    explored = set()
    cutoff = 0

    while True:
        if len(frontier) == 0:
            if cutoff > 1:
                return "No Solution"
            else:
                return "Failure"
        node = frontier.pop()
        frontierSet.remove(node)
        explored.add(node)
        depth = node.depth
        expanded += 1

        for child in node.children():
            if child not in explored and child not in frontierSet and depth < limit:
                if child.state.isSolvedState():
                    return Solution(child.path(), expanded)
                frontier.append(child)
                frontierSet.add(child)
            elif depth == limit:
                cutoff += 1

def iterative_deepening(puz):
    out = "No Solution"
    count = 0
    while out == "No Solution":
        count += 1
        out = depth_limited(puz, count)
    if out == "Failure":
        return "No Solution"
    return out


def breadthFirst(puz):
    expanded = 0
    start = Node(puz.state)
    frontier = deque([start])
    frontierSet = set([start])
    explored = set()

    while True:
        if len(frontier) == 0:
            return "No Solution"

        node = frontier.popleft()
        frontierSet.remove(node)
        explored.add(node)
        expanded += 1

        for child in node.children():
            if child not in explored and child not in frontierSet:
                if child.state.isSolvedState():
                    return Solution(child.path(), expanded)
                frontier.append(child)
                frontierSet.add(child)

def bidirectional(puz):
    def _join(lNode, rNode):
        lPath = lNode.path()
        rPath = rNode.invertedPath()[1:]
        rPath[0].parent = lPath[-1]
        return lPath + rPath

    expanded = 0
    left = Node(puz.state)
    right = Node(State.Solved())

    frontierL = deque([left])
    frontierLSet = {left.key: left}
    frontierR = deque([right])
    frontierRSet = {right.key: right}

    expL = {}
    expR = {}

    while True:
        if len(frontierL) == 0 or len(frontierR) == 0:
            return "No Solution"

        nodeL = frontierL.popleft()
        del frontierLSet[nodeL.key]

        nodeR = frontierR.popleft()
        del frontierRSet[nodeR.key]

        expL[nodeL.key] = nodeL
        expR[nodeR.key] = nodeR

        expanded += 1
        for child in nodeL.children():
            if child.key not in expL and child.key not in frontierLSet:
                frontierL.append(child)
                frontierLSet[child.key] = child
                if child.key in frontierRSet:
                    return Solution(_join(child, frontierRSet[child.key]), expanded)

        expanded += 1
        for child in nodeR.children():
            if child.key not in expR and child.key not in frontierRSet:
                frontierR.append(child)
                frontierRSet[child.key] = child
                if child.key in frontierLSet:
                    return Solution(_join(frontierLSet[child.key], child), expanded)

def bestFirst(puz, f):
    expanded = 0
    start = Node(puz.state)

    frontier = pqueue()
    frontier.push(start, 0)

    explored = set()

    while True:
        if len(frontier) == 0:
            return "No Solution"

        node = frontier.pop()

        if node.state.isSolvedState():
            return Solution(node.path(), expanded)

        explored.add(node)
        expanded += 1

        for child in node.children():
            if child not in explored and child not in frontier:
                frontier.push(child, f(child))
            elif child in frontier and f(child) < frontier.priority(child):
                frontier.push(child, f(child))

def aStar(puz, h):
    def f(node):
        return node.cost + h(node)
    return bestFirst(puz, f)
