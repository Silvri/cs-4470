import itertools
import heapq

class pqueue:
    __REMOVED = '<removed>'
    
    def __init__(self):
        self._size = 0
        self._heap = []
        self._counter = itertools.count()
        self._entryset = {}
        
    def push(self, entry, priority):
        'Add a new entry or update priority of existing entry'
        if entry in self._entryset:
            self.remove(entry)
        count = next(self._counter)
        node = [priority, count, entry]
        heapq.heappush(self._heap, node)
        self._entryset[entry] = node
        self._size += 1
        
    def pop(self):
        while self._heap:
            priority, count, entry = heapq.heappop(self._heap)
            if entry is not pqueue.__REMOVED:
                self._size -= 1
                del self._entryset[entry]
                return entry
        raise KeyError('pop from an empty priority queue')
        
    def remove(self, entry):
        e = self._entryset.pop(entry)
        e[-1] = pqueue.__REMOVED
        self._size -= 1
    
    def priority(self, entry):
        priority, count, entry = self._entryset[entry]
        return priority
    
    def __len__(self):
        return self._size
    
    def __contains__(self, entry):
        return entry in self._entryset
    