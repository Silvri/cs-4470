# Uninformed and Informed Searches - 8 puzzle

## Assets
The following assets are provided
* main.py - main driver
* searches.py - algorithm logic
* puzzle.py - manages game board and states
* pqueue.py - priority queue for use in searches
* board.txt - sample input board
* heuristic_writeup.txt - short explenation of custom heuristic

## Usage instructions
To run the searches execute one of the following commands from within project directory:
* `python main.py -r` // Executes the searches on a randomized puzzle
*  `python main.py <path_to_file>` // Executes searches on pre-made board(see board.txt)
