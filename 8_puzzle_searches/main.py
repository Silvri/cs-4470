from puzzle import Puzzle
from functools import partial

import searches
import time
import sys
solved = '12345678_'

# p = Puzzle()
# # p.state = State('8672543_1', 7)
# p.shuffle()
# # try:

def taxicab(node):
    s = node.state.id
    def dist(idx1, idx2):
        return abs(idx1 // 3 - idx2 // 3) + abs(idx1 % 3 - idx2 % 3)
    return sum(dist(s.index(c), solved.index(c)) for c in solved)

def custom_heuristic(node):
    s = node.state.id
    return sum([1 for x in s if x != solved[s.index(x)]])

def evalSearch(name, search):
    start = time.time()
    sol = search()
    elapsed = time.time() - start
    print('-', name, '-')
    print('Time:', elapsed * 1000, 'ms')
    print(sol)

if __name__ == '__main__':
    if len(sys.argv):
        if(sys.argv[1] == '-r'):
            p = Puzzle()
            p.shuffle()
        else:
            with open(sys.argv[1]) as f:
                states = "".join(f.read().splitlines())
            p = Puzzle(states)
        evalSearch('DepthFirst', partial(searches.depth_first, p))
        evalSearch('DepthLimited', partial(searches.depth_limited, p, 50))
        evalSearch('IterativeDeepening', partial(searches.iterative_deepening, p))
        evalSearch('Breadth-First', partial(searches.breadthFirst, p))
        evalSearch('Bidirectional', partial(searches.bidirectional, p))
        evalSearch('Greedy Best-First - Manhattan', partial(searches.bestFirst, p, taxicab))
        evalSearch('Greedy Best-First - Customer Heuristic', partial(searches.bestFirst, p, custom_heuristic))
        evalSearch('A* - Manhattan', partial(searches.aStar, p, taxicab))
        evalSearch('A* - Custom Heuristic', partial(searches.aStar, p, custom_heuristic))
