When designing our heuristic we wanted to look at the difference of the current
state and the goal state. We believed that by providing a score based on the
difference, that the overall change needed in the board would provide a more
effective heuristic. To get this heuristic we compared tiles between the the
current state and the goal. If the tiles were different, a score of 1 was given
to that tile. The tiles were then summed up and returned as the score.

The heuristic did not turn out how we thought however as the speeds can be as
high as 4  times slower using the custom heuristic, this is most likely due to
the fact that Manhattan always finds the shortest path which is more useful to
greedy-style algorithms.
